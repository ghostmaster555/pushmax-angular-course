## Project description
This app is a kinda grinder game like cookie clicker or virtual begga.
The aim is to reach as high cat level as you can.

The cat eats sausages and it's bellyfull grows as well as it's level.
Don't feed the cat and it'll begin starving and losing lvl.
The higher cat's lvl is the more difficult to sustain it's bellyfull.

Click "Buy sausages!" to fill the fridge.
Click to the cat to feed it.

As cat's lvl grows, it can give you respect points. Spend them to upgrade your equipment.

_______________
## Setup for Development

Welcome to the example app used in the
[Setup for Development](https://webdev.dartlang.org/angular/guide/setup) page
of [Dart for the web](https://webdev.dartlang.org).

You can run a [hosted copy](https://webdev.dartlang.org/examples/quickstart) of this
sample. Or run your own copy:

1. Create a local copy of this repo (use the "Clone or download" button above).
2. Get the dependencies: `pub get`
3. Get the webdev tool: `pub global activate webdev`
4. Launch a development server: `webdev serve`
5. In a browser, open [http://localhost:8080](http://localhost:8080)

---

*Note:* The content of this repository is generated from the
[Angular docs repository][docs repo] by running the
[dart-doc-syncer](//github.com/dart-lang/dart-doc-syncer) tool.
If you find a problem with this sample's code, please open an [issue][].

[docs repo]: //github.com/dart-lang/site-webdev/tree/master/examples/ng/doc/quickstart
[issue]: //github.com/dart-lang/site-webdev/issues/new?title=[master]%20examples/ng/doc/quickstart
