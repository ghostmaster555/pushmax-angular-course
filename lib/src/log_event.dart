class EventLevel {
  final String _value;
  const EventLevel(this._value);

  static const EventLevel info = EventLevel('info');
  static const EventLevel warning = EventLevel('warning');
  static const EventLevel achievement = EventLevel('achievement');

  @override
  String toString() => _value;
}

class LogEvent {
  String message;
  EventLevel level;

  LogEvent(this.message, [this.level = EventLevel.info]);
}