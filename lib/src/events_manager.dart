import 'dart:async';
import 'package:angular_app/src/log_event.dart';

class EventsManager {
  List<LogEvent> events = [];
  int messageLifetimeMilliseconds = 7000;

  void pushEvent(String message, [EventLevel level]) {
    events.add(LogEvent(message, level));
    Timer(Duration(milliseconds: messageLifetimeMilliseconds), _removeOldestMessage);
  }

  void _removeOldestMessage() {
    if (events.length < 1) {
      return;
    }

    events.removeAt(0);
  }

}