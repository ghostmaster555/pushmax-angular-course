class RespectManager {
  int totalRespect = 0;

  void addRespect(int count) {
    totalRespect += count;
  }

  bool consumeRespect(int count) {
    if (count > totalRespect) {
    return false;
  }

  totalRespect -= count;

    return true;
  }
}