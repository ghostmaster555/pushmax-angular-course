import 'package:angular/angular.dart';
import 'package:angular_app/components/cat_kingdom.dart';

@Component(
  selector: 'my-app',
  templateUrl: 'app_component.html',
  directives: [CatKingdom],
  styleUrls: ['app_component.css'],
)
class AppComponent {
  var name = 'Angular';
}
