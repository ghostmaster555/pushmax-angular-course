import 'dart:async';
import 'package:angular/angular.dart';
import 'package:angular_app/src/events_manager.dart';
import 'package:angular_app/src/log_event.dart';
import 'package:angular_app/src/respect_manager.dart';

@Component(
  selector: 'the-cat',
  templateUrl: 'the_cat.html',
  styleUrls: ['the_cat.css'],
)
class TheCat implements OnInit, OnDestroy {
  final List<int> bellyfulCapacityEachLevel = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];//[10, 20, 35, 60, 100, 150, 250, 500];
  final List<int> respectGivenEachLevel = [0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10];
  final EventsManager _eventsManager;

  int sausagesEaten = 0;
  int level = 0;
  int maxLevel = 20;
  int currentBellyful = 0;
  int respectTimerPeriodMilliseconds = 2000;
  int hungerTimerPeriodMilliseconds = 4000;

  int get catWidth => 195 + 30 * level;

  Timer respectTimer;
  Timer hungerTimer;

  int get bellyfulCapacity => bellyfulCapacityEachLevel[level];
  int get respectIncome => respectGivenEachLevel[level];

  final RespectManager _respectManager;

  TheCat(this._respectManager, this._eventsManager);

  void giveSausages(int count) {
    sausagesEaten += count;
    currentBellyful += count;

    // refreshing the timer
    hungerTimer.cancel();
    _createHungerTimer();

    if (currentBellyful >= bellyfulCapacity) {
        if (level + 1 > maxLevel) {
          level = maxLevel;
          currentBellyful = bellyfulCapacity;
          return;
        }

      currentBellyful = currentBellyful - bellyfulCapacity;
      level ++;
      _eventsManager.pushEvent('The Cat reached level $level!!', EventLevel.achievement);
    }
  }

  void _getHungry() {
    if (currentBellyful != 0) {
      _eventsManager.pushEvent('The Cat is hungry, $level bellyful points lost!', EventLevel.warning);
    }

    currentBellyful -= level;

    if (currentBellyful < 0) {
      if (level > 0) {
        level --;
        currentBellyful = (bellyfulCapacity / 2).floor();
        _eventsManager.pushEvent('THE INCREDIBLE HUNGER made The Cat downgrade to level $level :((', EventLevel.warning);
        return;
      }

      currentBellyful = 0;
    }
  }

  void _createHungerTimer() {
    hungerTimer = Timer.periodic(
        Duration(milliseconds: hungerTimerPeriodMilliseconds),
            (_) {
          _getHungry();
        }
    );
  }

  @override
  void ngOnInit() {
    respectTimer = Timer.periodic(
      Duration(milliseconds: respectTimerPeriodMilliseconds),
      (_) {
        final respectPointsEarned = respectGivenEachLevel[level];
        if (respectPointsEarned > 0) {
          _eventsManager.pushEvent('${respectGivenEachLevel[level]} respect points earned!');
          _respectManager.addRespect(respectPointsEarned);
        }
      }
    );

    _createHungerTimer();
  }

  @override
  void ngOnDestroy() {
    respectTimer.cancel();
    hungerTimer.cancel();
  }

}