import 'package:angular/angular.dart';
import 'package:angular_app/src/respect_manager.dart';

@Component(
  selector: 'respect-block',
  templateUrl: 'respect_block.html',
  styleUrls: ['respect_block.css'],
)
class RespectBlock {
  final RespectManager _respectManager;

  int get totalRespect => _respectManager.totalRespect;

  RespectBlock(this._respectManager);
}