import 'dart:async';
import 'package:angular/angular.dart';
import 'package:angular_app/src/events_manager.dart';

@Component(
    selector: 'shop',
    templateUrl: 'shop.html',
    styleUrls: ['shop.css'],
)
class Shop {
  int sausagesEachClick = 1;
  final EventsManager _eventsManager;

  StreamController<int> _shopController = StreamController();

  Shop(this._eventsManager);

  @Output('buy-sausages')
  Stream<int> get buySausages => _shopController.stream;

  void onBuySausages() {
    _shopController.add(sausagesEachClick);
    _eventsManager.pushEvent('$sausagesEachClick sausages bought!');
  }
}