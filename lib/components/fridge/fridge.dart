import 'package:angular/angular.dart';
import 'package:angular_app/src/events_manager.dart';
import 'package:angular_app/src/log_event.dart';
import 'package:angular_app/src/respect_manager.dart';

@Component(
    selector: 'fridge',
    templateUrl: 'fridge.html',
    styleUrls: ['fridge.css'],
)
class Fridge {
  int level = 0;
  int maxLevel = 4;
  int sausagesCount = 0;

  List<int> upgradeCostByLevel = [50, 100, 200, 500, 1000];
  List<int> capacityByLevel = [25, 50, 100, 200, 500];

  String get upgradeButtonLabel {
    return isMaxLevel ?
        'MAX LEVEL' :
        'Upgrade! ($upgradeCost respect points)';
  }

  int get upgradeCost => upgradeCostByLevel[level];
  int get capacity => capacityByLevel[level];
  bool get isMaxLevel => level == maxLevel;
  bool get canUpgrade => _respectManager.totalRespect >= upgradeCost && !isMaxLevel;

  final RespectManager _respectManager;
  final EventsManager _eventsManager;

  Fridge(this._respectManager, this._eventsManager);

  void putSausages(int count) {
    sausagesCount += count;

    if (sausagesCount > capacity) {
      sausagesCount = capacity;
    }
  }

  void upgrade() {
    if (!canUpgrade) {
      return;
    }

    _respectManager.consumeRespect(upgradeCost);
    level ++;

    if (level > maxLevel) {
      level = maxLevel;
      return;
    }

    _eventsManager.pushEvent('Fridge is upgraded to level $level!!', EventLevel.achievement);
  }

  int takeSausages(count) {
    int result = 0;

    if (sausagesCount <= count) {
      result = sausagesCount;
      _eventsManager.pushEvent('Fridge is empty!!', EventLevel.warning);
    } else {
      sausagesCount -= count;
      result = count;
    }

    return result;
  }
}