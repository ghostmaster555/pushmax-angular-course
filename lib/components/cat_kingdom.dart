import 'package:angular/angular.dart';
import 'package:angular_app/components/events_log/events_log.dart';
import 'package:angular_app/components/respect_block/respect_block.dart';
import 'package:angular_app/components/the_cat/the_cat.dart';
import 'package:angular_app/components/fridge/fridge.dart';
import 'package:angular_app/components/shop/shop.dart';
import 'package:angular_app/src/events_manager.dart';
import 'package:angular_app/src/respect_manager.dart';

@Component(
  selector: 'cat-kingdom',
  templateUrl: 'cat_kingdom.html',
  styleUrls: ['cat_kingdom.css'],
  directives: [
    TheCat,
    Fridge,
    Shop,
    RespectBlock,
    EventsLog,
  ],
  providers: [
    ClassProvider<RespectManager>(RespectManager),
    ClassProvider<EventsManager>(EventsManager),
  ]
)
class CatKingdom {
  @ViewChild(TheCat)
  TheCat theCat;

  @ViewChild(Fridge)
  Fridge fridge;

  @ViewChild(RespectBlock)
  RespectBlock respectBlock;

  void onBuySausages(int count) {
    fridge.putSausages(count);
  }
  
  void onCatClicked() {
    theCat.giveSausages(fridge.takeSausages(1));
  }
}