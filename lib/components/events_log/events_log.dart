import 'package:angular/angular.dart';
import 'package:angular_app/src/log_event.dart';
import 'package:angular_app/src/events_manager.dart';

@Component(
  selector: 'events-log',
  templateUrl: 'events_log.html',
  styleUrls: ['events_log.css'],
  directives: [
    NgFor,
    NgClass
  ]
)
class EventsLog {
  final EventsManager _manager;

  List<LogEvent> get events => _manager.events.reversed.toList();

  EventsLog(this._manager);

  String constructLogEventClass(EventLevel level) => 'event-record__${level}';
}